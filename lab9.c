1) #include <stdio.h>
   int main()
    {
   int x, y, *a, *b, temp;

   printf("Enter the value of x and y\n");
   scanf("%d%d", &x, &y);

   printf("Before Swapping\nx = %d\ny = %d\n", x, y);
   
   a = &x;
   b = &y;
   
   temp = *b;
   *b   = *a;
   *a   = temp;

   printf("After Swapping\nx = %d\ny = %d\n", x, y);
   
   return 0;
    }

2)#include <stdio.h> 
  int main() 
     { 
    int x, y; 
    printf("Enter Value of x "); 
    scanf("%d", &x); 
    printf("\nEnter Value of y "); 
    scanf("%d", &y); 
     int variable = x; 
    x = y; 
    y = variable; 
  
    printf("\nAfter Swapping: x = %d, y = %d", x, y); 
    return 0; 
  } 



